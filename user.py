from datetime import datetime
import falcon
import json
from models import Base, Engine
from sqlalchemy.orm import sessionmaker
import sqlalchemy

session = sessionmaker()
session.configure(bind=engine)
Base.metadata.bind = engine

class Resource:
    pass
