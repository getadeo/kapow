from datetime import datetime
import falcon
import json
from models import Team, Base, engine
from sqlalchemy.orm import sessionmaker
import sqlalchemy

session = sessionmaker()
session.configure(bind=engine)
Base.metadata.bind = engine


class Resource:
    def on_post(self, req, resp):
        s = session()
        name = req.get_param('name')
        if not name:
            resp.body = json.dumps({'error': 'Team name is required.'})
            resp.status = falcon.HTTP_403
        else:
            t = Team(name=name)
            s.add(t)
            s.commit()
            s.close()
            resp.body = json.dumps({"message": "added"})
            resp.status = falcon.HTTP_OK

    def on_get(self, req, resp, team_id):
        s = session()
        t = s.query(Team).filter_by(id=team_id).first()
        data = {
                'id': t.id,
                'name': str(t.name)
        }
        s.close()
        resp.body = json.dumps(data)

class Collection:

    def on_get(self, req, resp):
        s = session()
        teams = s.query(Team).all()
        s.close()
        resp.body = json.dumps({'teams': map(lambda t: t.to_dict(), teams)})

