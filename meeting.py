from datetime import datetime
import falcon
import json
from models import Meeting, Base, engine
from sqlalchemy.orm import sessionmaker
import sqlalchemy

session = sessionmaker()
session.configure(bind=engine)
Base.metadata.bind = engine


class Resource:

    def on_post(self, req, resp):
        s = session()
        content = req.get_param('content')
        start_time = req.get_param('start_time')
        end_time = req.get_param('end_time')
        m = Meeting(
                content=content,
                created_at=datetime.utcnow(),
                start_time=start_time, 
                end_time=end_time
        )
        s.add(m)
        s.commit()
        s.close()
        resp.body = json.dumps({"message": "posted"})

    def on_get(self, req, resp, meeting_id):
        s = session()
        m = s.query(Meeting).filter_by(id=meeting_id).first()
        data = {
                "id": m.id,
                "content": m.content,
                "start_time": str(m.start_time),
                "end_time": str(m.end_time),
                "created_at": m.created_at.isoformat()
        }
        s.close()
        resp.body = json.dumps({'result': data})
        resp.status = falcon.HTTP_OK

class Collection:

    def on_get(self, req, resp):
        s = session()
        meetings = s.query(Meeting).all()
        s.close()
        resp.body = json.dumps({'meetings': map(lambda m: m.to_dict(), meetings)})
        resp.status = falcon.HTTP_OK

