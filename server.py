import falcon
import meeting
import team

api = falcon.API()

# Meeting API Routes
api.add_route('/meetings', meeting.Collection())
api.add_route('/meetings/add', meeting.Resource())
api.add_route('/meetings/{meeting_id}', meeting.Resource())

# Team API Routes
api.add_route('/teams', team.Collection())
api.add_route('/teams/add', team.Resource())
api.add_route('/teams/{team_id}', team.Resource())
