import config
from markdown2 import markdown
import sendgrid

sg = sendgrid.SendGridClient(config.SENDGRID_API_KEY)
message = sendgrid.Mail()

class Email(object):

    def __init__(self, emails, subject, content):
        self.emails = emails
        self.content = content
        self.subject = subject

    def send(self):
        message.add_to(self.emails)
        message.set_subject(self.subject)
        message.set_html(markdown(self.content))
        message.set_from('<Kapow App> kapow@infoshiftinc.com')
        status, msg = sg.send(message)
        return {
                'status': status,
                'msg':msg
        }
