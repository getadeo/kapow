from sqlalchemy import Table, Column, Integer, Text, String, DateTime, Time, MetaData, create_engine, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
import config

engine = create_engine(config.SQLALCHEMY_URI, pool_recycle=3600)

metadata = MetaData()

Base = declarative_base()

class Meeting(Base):
    __tablename__ = 'meeting'
    id = Column(Integer, primary_key=True)
    start_time = Column(Time)
    end_time = Column(Time)
    content = Column(Text)
    created_at = Column(DateTime)

    def to_dict(self):
        return {
                'id': self.id,
                'start_time': str(self.start_time),
                'end_time': str(self.end_time),
                'content': self.content,
                'created_at': self.created_at.isoformat()
        }

class User(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    first_name = Column(String(68))
    last_name = Column(String(68))
    email = Column(String(68))
    team_id = Column(Integer, ForeignKey('team.id'))

    def to_dict(self):
        return {
                'id': self.id,
                'first_name': self.first_name,
                'last_name': self.last_name,
                'email': self.email
        }

class Team(Base):
    __tablename__ = 'team'
    id = Column(Integer, primary_key=True)
    name = Column(String(68))
    users = relationship("User")

    def to_dict(self):
        return {
                'id': self.id,
                'name': self.name
        }


